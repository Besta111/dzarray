import java.awt.*;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int i = 0, n, j = 0;
        System.out.print("\nВведите число из скольки чисел будет состоять массив");
        n = scan.nextInt();
        int[] mas1 = new int[n];
        int[] mas2 = new int[n];

        System.out.printf("\nВведите %d элемента массива", n);
        for (i = 0; i < n; i++) {
            mas1[i] = scan.nextInt();
        }
        for (i = 0; i < n; i++) {
            System.out.printf(" %d ", mas1[i], "\n");
        }
        for (i = 0; i < n; i++) {
            if (mas1[i] != 0) {
                mas2[j] = mas1[i];
                j++;
            }
        }
        System.out.print("\n");
        for (j = 0; j < n; j++) {
            System.out.print(mas2[j]);
        }
    }

}